﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterGun : MonoBehaviour {

    public GameObject sphereBall;

    public GameObject arrow_emitter;

    public float arrow_force;

    float speed = 5.0F;
    public int maxShoot = 5;

    public Vector3 lookPos;
    private Vector3 lookP;

    public Transform target;

    private float minAngleRot = -45.0f;
    private float maxAngleRot =  45.0f;

    Vector3 mouseWorldPosition ;

    // Use this for initialization
    void Awake () 
    {
     	}
	
	// Update is called once per frame
	void Update () 
    {
        

        //this.transform.rotation.x >= -45.0f && this.transform.rotation.x <= 45.0f

        if (Input.GetMouseButton(0) && minAngleRot <= 45 && minAngleRot >= -45)
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 10.0f;
            Vector3 lookPos = Camera.main.ScreenToWorldPoint(mousePos);

            Quaternion targetRot = Quaternion.LookRotation(lookPos - transform.position, Vector3.forward);

            /*

            targetRot.z = 0;
            targetRot.y = 0;
            transform.rotation.z = 0;
            transform.rotation.y = 0;

            transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * 30);

            */

            lookPos.z = 0;

            transform.LookAt(lookPos);

        }

        if (Input.GetMouseButtonUp(0))
        {
            GameObject temp_arrow_holder;
            temp_arrow_holder = Instantiate(sphereBall, arrow_emitter.transform.position, arrow_emitter.transform.rotation) as GameObject;

            Rigidbody temp_rigidbody;
            temp_rigidbody = temp_arrow_holder.GetComponent<Rigidbody>();

            temp_rigidbody.AddForce(transform.forward * arrow_force);

            Destroy(temp_arrow_holder, 4.0f);

        }



        /*
         
        //Rotate
        Vector3 dirToLook = lookPos - transform.position;
        dirToLook.y = 0;

        Quaternion targetRot = Quaternion.LookRotation(dirToLook);

        Debug.Log(lookPos.x + " " + transform.position.x);

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * 15);

        //Aim
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            Vector3 lookP = hit.point;
            lookP.z = transform.position.z;
            lookPos = lookP;
        }

        */




        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -35.0f, 35.0f), Mathf.Clamp(transform.position.y, 1.0f, 8.0f), 0);
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.down * speed * Time.deltaTime;
        }

        /*

        if (Input.GetKeyDown(KeyCode.Space) && maxShoot <= 10)
        {
            Vector3 position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);     
            Instantiate(sphereBall, position, Quaternion.identity);
            //Instantiate(sphereBall, position, Quaternion.Euler(0,0,-90));

            Instantiate(sphereBall, position, sphereBall.transform.rotation);

            maxShoot++;
        }		

        */

	}
}
