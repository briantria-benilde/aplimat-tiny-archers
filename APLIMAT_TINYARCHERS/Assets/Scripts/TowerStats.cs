﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TowerStats : MonoBehaviour {
	public static int TowerMaxHealth = 100;
	public Slider HealthSlider;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		HealthSlider.value = TowerMaxHealth;

		if (TowerMaxHealth < 1) {
			Application.LoadLevel (0);
		}
	}

}
