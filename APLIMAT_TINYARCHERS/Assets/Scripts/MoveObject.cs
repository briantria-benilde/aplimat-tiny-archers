﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MoveObject : MonoBehaviour {

    public float enemyDistance;
    public float speed;
    public Transform tower;
    float timer = 0.0f;
    float maxTime = 3.0f;
	public  int enemyHealth = 10;
    // Use this for initialization
	public static float xPos;
    void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		xPos = this.transform.position.x;

        timer += 0.07f;

		if (timer > maxTime)
		{
			TowerStats.TowerMaxHealth -= 1;
			Debug.Log(SpawnEnemy.MaxEnemyCount);
			timer = 0;
			// Attack();
		}
        enemyDistance = Vector3.Distance(tower.position, transform.position);

        if (enemyDistance > 5.3f)
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }
		if (enemyHealth < 1) {
			Destroy (this.gameObject);
			SpawnEnemy.MaxEnemyCount -= 1;
		}
	
        if (GameObject.Find ("Enemy(Clone)").transform.position.x - GameObject.Find ("Sphere(Clone)").transform.position.x < 1 && GameObject.Find ("Enemy(Clone)").transform.position.y - GameObject.Find ("Sphere(Clone)").transform.position.y < 1 )  {
			Destroy (this.gameObject);
		}
       /*if (enemyDistance < 5.3f)
        {
            if (timer > maxTime)
            {
                TowerStats.TowerMaxHealth -= 1;
				Debug.Log(TowerStats.TowerMaxHealth);
                timer = 0;
                // Attack();
            }

        }*/
    }
}
