﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour {
	public GameObject Enemy;
	public static int MaxEnemyCount;

	// Use this for initialization
	void Start () {
		
		InvokeRepeating("Spawn", 2.0f, 1.3f);

	}

	// Update is called once per frame
	void Spawn()
	{
		if (MaxEnemyCount <= 20)
		{

			Vector3 position = new Vector3(transform.position.x,transform.position.y,transform.position.z);
			Instantiate(Enemy, position, Quaternion.identity);

			//cm.transform++;
			MaxEnemyCount++;
		}
	}
}