﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorMeth : MonoBehaviour {

    public Vector3 velocity;
    private Vector3 gravity = new Vector3(0.0f, 1.5f, 0.0f);

    private Transform spherePos;


    public float Speed;

    private ShooterGun shoot;

	// Use this for initialization
	void Start () 
    {
        spherePos = GetComponent<Transform>();
        velocity = new Vector3(0.5f, 0f, 0f);
        shoot = GetComponent<ShooterGun>();
	}
	
	// Update is called once per frame
	void Update () 
    {
       // spherePos.Translate( spherePos.forward * Speed * Time.deltaTime);


        transform.position += transform.right;


        
	}
    void OnBecameInvisible()
    {
        Destroy(this.gameObject);
        
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {

            Destroy(this.gameObject);
            shoot.maxShoot--;
        }
      
    }
}
